import json
import socket
import sys

class Track(object):
    def __init__(self, track):
        self.track = track
        previous = 0
        for piece in self.track['pieces']:
            if 'length' not in piece.keys():
                piece['length'] = piece['angle'] * 3.14 * piece['radius'] / 180.0
            piece['previous_acc_length'] = previous
            previous += piece['length']
    def throttle(self,piece_index, speed, position):
        current_piece = self.track['pieces'][piece_index]
        next_piece =  self.track['pieces'][(piece_index + 1) % len(self.track['pieces'])]
        second_next_piece =  self.track['pieces'][(piece_index + 2) % len(self.track['pieces'])]

        bend_condition = ('angle' in second_next_piece.keys() or
                          'angle' in next_piece.keys() or
                          'angle' in current_piece.keys())

        if bend_condition:
            car_angle = abs(position['angle'])
            bend_angle = abs(current_piece.get('angle',0) +
                             next_piece.get('angle',0))
            if car_angle > 5 and bend_angle > 20:
                return self.compute_throttle('low', speed)
            else:
                return self.compute_throttle('bend', speed)
        else:
            return self.compute_throttle('max', speed)

    def compute_throttle(self, goal, speed):
        print 'speed goal: {0}'.format(goal)
        if goal == 'low':
            # Our goal is to slow down a lot, due to some emergency
            # condition (for example, large angle values
            return 0.2
        elif goal == 'bend':
            if speed < 4:
                return 0.65
            if speed < 5:
                return 0.55
            elif speed < 6:
                return 0.4
            elif speed < 8:
                return 0.3
            else:
                return 0.1
        else:
            # goal is "max"
            return 1

    def current_piece(self,position):
        return self.track['pieces'][position['piecePosition']['pieceIndex']]

        
class NoobBot(object):

    last_pos = 0
    track = None
    current_tick = 0
    speed = 0
    
    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()
        
    def on_game_init(self, data):
        print('Welcome to {0} ({1})'.format(data['race']['track']['name'], data['race']['track']['id']))
        self.track = Track(data['race']['track'])
        self.ping()

    def on_car_positions(self, data):
        our_positions = [i for i in data if i['id']['name'] == 'elk lasH']

        for position in our_positions:
            current_piece = self.track.current_piece(position)
            current_position = current_piece['previous_acc_length'] + position['piecePosition']['inPieceDistance']

            if current_position >= self.last_pos:
                self.speed = current_position - self.last_pos

            self.last_pos = current_position
            
            print('piece {0}, offset {1}, current position {2}, speed {3}'
                  .format(position['piecePosition']['pieceIndex'],
                          position['piecePosition']['inPieceDistance'],
                          current_position,
                          self.speed)),

            print('[{0},{1}]'.format(position['angle'],current_piece.get('angle',None))),
        self.throttle(self.track.throttle(position['piecePosition']['pieceIndex'], self.speed, position))

    def on_crash(self, data):
        print("Someone crashed")
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'gameInit': self.on_game_init,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
